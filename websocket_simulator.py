#!/usr/bin/python

import websocket
import json, sys
from datetime import datetime
##### Simulates kiosk as WS client

userIdQuery = '?userId='
userId = ''

URL = 'wss://i93po5s0lh.execute-api.us-west-1.amazonaws.com/Prod'
# try:
#     import thread
# except ImportError:
#     import _thread as thread
# import time
def on_message(ws, message):
    curr_time = datetime.now()
    formatted_time = curr_time.strftime('%S')

    print('Message received {}'.format(formatted_time))

    print(message)
    print('\n\n')

def on_error(ws, error):
    print("error occured")
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    print("Connected")
    # def run(*args):
    #     for i in range(3):
    #         time.sleep(1)
    #         ws.send("Hello %d" % i)
    #     time.sleep(1)
    #     # ws.close()
    #     print("thread terminating...")
    # thread.start_new_thread(run, ())
    connId = {
        "action": "getConnectionId"
    }
    ws.send(json.dumps(connId))

    msg = { 
        "action": "sendmessage", 
		"data": "kioskConnected", 
		"userId": userId
	};

    ws.send(json.dumps(msg))


if __name__ == "__main__":
    userId = sys.argv[-1]
    print('User ID is {}'.format(userId))
    URL += userIdQuery + userId
    print('New URL is {}'.format(URL))
    # websocket.enableTrace(True)
    ws = websocket.WebSocketApp(URL,
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()

    
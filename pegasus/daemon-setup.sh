#!/bin/bash


echo "Copying files...."
sudo cp /home/dropwater/kiosk_scripts/pegasus/celery.service /etc/systemd/system/celery.service 
sudo cp /home/dropwater/kiosk_scripts/pegasus/pegasus-worker.service /etc/systemd/system/pegasus-worker.service 
sudo cp /home/dropwater/kiosk_scripts/pegasus/pegasus.service /etc/systemd/system/pegasus.service 
sudo cp /home/dropwater/kiosk_scripts/pegasus/redis-server.service /etc/systemd/system/redis-server.service 


echo "Reloading systemctl daemon"
sudo systemctl daemon-reload

echo "Enabling services."

sudo systemctl enable redis-server 
sudo systemctl enable celery 
sudo systemctl enable pegasus
sudo systemctl enable pegasus-worker


sudo systemctl start redis-server 
sudo systemctl start celery 
sudo systemctl start pegasus
sudo systemctl start pegasus-worker



sudo supervisorctl reload
sudo service supervisor restart



supervisorctl start myproject
supervisorctl stop myproject
supervisorctl restart myproject


sudo supervisorctl reread
sudo supervisorctl update

sudo supervisorctl start celery 
sudo supervisorctl start pegasus
sudo supervisorctl start pegasus-worker 
sudo supervisorctl start redis 






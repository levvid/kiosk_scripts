import os


def main():
    # print("pulling kiosk scripts\n\n")
    # os.system("cd ~/kiosk_scripts/ && git pull")
    # print("Checking out release")
    # os.system("cd ~/Desktop/drop-water-kiosk-server/ && git fetch --all && git checkout release && git pull origin release")
    # print("\n\nRunning npm i\n\n")
    # os.system("cd ~/Desktop/drop-water-kiosk-server/ && npm i")
    os.system("cd ~/kiosk_scripts/")

    ######################## KIOSK Scripts #######################
    # print("\n\nCopying files\n\n")
    # os.system("cp ~/kiosk_scripts/KioskPrepareDrink.webm ~/Desktop/drop-water-kiosk-server/public/videos/KioskPrepareDrink.webm")
    # os.system("cp ~/kiosk_scripts/Pour.m4a ~/Desktop/drop-water-kiosk-server/public/audioFiles/Pour.m4a")
    # os.system("cp ~/kiosk_scripts/Done.m4a ~/Desktop/drop-water-kiosk-server/public/audioFiles/Done.m4a")
    # os.system("cp ~/kiosk_scripts/KioskPrepareDrink.mp4 ~/Desktop/drop-water-kiosk-server/public/videos/KioskPrepareDrink.mp4")
    # os.system("cp ~/kiosk_scripts/ScreenSaver.mp4 ~/Desktop/drop-water-kiosk-server/public/videos/ScreenSaver.mp4")

    # print("\n\nGiving permissions\n\n")
    # os.system("chmod a+rwx ~/Desktop/drop-water-kiosk-server/public/videos/KioskPrepareDrink.webm")
    # os.system("chmod a+rwx ~/Desktop/drop-water-kiosk-server/public/audioFiles/Pour.m4a")
    # os.system("chmod a+rwx ~/Desktop/drop-water-kiosk-server/public/audioFiles/Done.m4a")
    # os.system("chmod a+rwx ~/Desktop/drop-water-kiosk-server/public/videos/KioskPrepareDrink.mp4")
    # os.system("chmod a+rwx ~/Desktop/drop-water-kiosk-server/public/videos/ScreenSaver.mp4")
    # print("\n\nDone updating everything\n\n")


    ################### MINI- Scripts #############################
    print("\n\nCopying files\n\n")
    os.system("cp ~/kiosk_scripts/KioskPrepareDrink.webm ~/Desktop/drop-mini-server/public/videos/KioskPrepareDrink.webm")
    os.system("cp ~/kiosk_scripts/Pour.m4a ~/Desktop/drop-mini-server/public/audioFiles/Pour.m4a")
    os.system("cp ~/kiosk_scripts/Done.m4a ~/Desktop/drop-mini-server/public/audioFiles/Done.m4a")
    os.system("cp ~/kiosk_scripts/KioskPrepareDrink.mp4 ~/Desktop/drop-mini-server/public/videos/KioskPrepareDrink.mp4")
    os.system("cp ~/kiosk_scripts/ScreenSaver.mp4 ~/Desktop/drop-mini-server/public/videos/ScreenSaver.mp4")

    print("\n\nGiving permissions\n\n")
    os.system("chmod a+rwx ~/Desktop/drop-mini-server/public/videos/KioskPrepareDrink.webm")
    os.system("chmod a+rwx ~/Desktop/drop-mini-server/public/audioFiles/Pour.m4a")
    os.system("chmod a+rwx ~/Desktop/drop-mini-server/public/audioFiles/Done.m4a")
    os.system("chmod a+rwx ~/Desktop/drop-mini-server/public/videos/KioskPrepareDrink.mp4")
    os.system("chmod a+rwx ~/Desktop/drop-mini-server/public/videos/ScreenSaver.mp4")
    print("\n\nDone updating everything\n\n")

if __name__ == "__main__":
    main()
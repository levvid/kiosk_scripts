import pymongo
import urllib
from datetime import datetime, timedelta 
import xlsxwriter 
import pytz
import sys
sys.path.insert(0,'..') # import modules from parent folder
from Email import sendEmailSES

STAGING_URL = "mongodb+srv://test_user:" + urllib.parse.quote_plus('drop@3507') + "@drop-water-bfrql.mongodb.net/DropWaterStaging?retryWrites=true&w=majority"
PROD_URL = "mongodb+srv://test_user:" + urllib.parse.quote_plus('drop@3507') + "@drop-water-bfrql.mongodb.net/DropWaterProduction?retryWrites=true&w=majority"
STAGING_DB = "DropWaterStaging"
PROD_DB = "DropWaterProduction"

# ENV = 'Staging'
ENV = 'Production'

if ENV == 'Staging':
    URL = STAGING_URL
    DB = STAGING_DB
else:
    URL = PROD_URL
    DB = PROD_DB

concourse_A = {
    'num_free_refills': 0,
    'num_paid_refills': 0,
    'total_paid_refills': 0,
    'num_compostable_bottles': 0,
    'total_paid_compostable': 0,
}

concourse_B = {
    'num_free_refills': 0,
    'num_paid_refills': 0,
    'total_paid_refills': 0,
    'num_compostable_bottles': 0,
    'total_paid_compostable': 0,
}

total_revenue_checker = 0
total_revenue_before_tax = 0

def createExcelbooks(start, end):
    today = datetime.now()
    filepath = ''
    # Prod
    if ENV == 'Staging':
        filepath = 'Drop Water Summary_{}_{}_{}.xlsx'.format(today.year,today.month,today.day)
    else:
        filepath = '/home/ubuntu/Drop Water Summary_{}_{}_{}.xlsx'.format(today.year,today.month,today.day)
    workbook = xlsxwriter.Workbook(filepath) 
    # workbook = xlsxwriter.Workbook('DropWater Summary_{}_{}_{}.xlsx'.format(today.year,today.month,today.day)) 

    # Dev
    worksheet = workbook.add_worksheet("Weekly Summary_{}-{}.xlsx".format(start.day, end.day)) 
    
    row = 0
    col = 0
    
    
    worksheet.write(row, col, 'Concourse A')

    worksheet.write(row+1, col, 'Free Refills')
    worksheet.write(row+1, col+1, concourse_A['num_free_refills'])
    
    worksheet.write(row+2, col, 'Paid Refills')
    worksheet.write(row+2, col + 1, concourse_A['num_paid_refills'])
    # worksheet.write(row+1, col, 'Paid Refills')
    if concourse_A['num_paid_refills'] > 0:
        worksheet.write(row+2, col+2, '(average cost ${:.2f})'.format(concourse_A['total_paid_refills']/concourse_A['num_paid_refills']))
    else:
        worksheet.write(row+2, col+2, '(average cost ${:.2f})'.format(concourse_A['total_paid_refills']))
    
    worksheet.write(row+3, col, 'Compostable Bottles')
    worksheet.write(row+3, col + 1, concourse_A['num_compostable_bottles'])
    if concourse_A['num_compostable_bottles'] > 0:
        worksheet.write(row+3, col + 2, '(average cost: ${:.2f})'.format(concourse_A['total_paid_compostable']/concourse_A['num_compostable_bottles']))
    else:
        worksheet.write(row+3, col + 2, '(average cost: ${:.2f})'.format(concourse_A['total_paid_compostable']))
    

    worksheet.write(row+5, col, 'Total Revenue Before Sales Tax')
    worksheet.write(row+5, col + 1, '${:.2f}'.format(concourse_A['total_revenue_before_tax']))

    worksheet.write(row+7, col, 'Concourse B')

    worksheet.write(row+8, col, 'Free Refills')
    worksheet.write(row+8, col+1, concourse_B['num_free_refills'])
    
    worksheet.write(row+9, col, 'Paid Refills')
    worksheet.write(row+9, col + 1, concourse_B['num_paid_refills'])
    # worksheet.write(row+1, col, 'Paid Refills')
    if concourse_B['num_paid_refills'] > 0:
        worksheet.write(row+9, col+2, '(average cost ${:.2f})'.format(concourse_B['total_paid_refills']/concourse_B['num_paid_refills']))
    else:
        worksheet.write(row+9, col+2, '(average cost ${:.2f})'.format(concourse_B['total_paid_refills']))
    
    worksheet.write(row+10, col, 'Compostable Bottles')
    worksheet.write(row+10, col + 1, concourse_B['num_compostable_bottles'])
    if concourse_B['num_compostable_bottles'] > 0:
        worksheet.write(row+10, col + 2, '(average cost: ${:.2f})'.format(concourse_B['total_paid_compostable']/concourse_B['num_compostable_bottles']))
    else:
        worksheet.write(row+10, col + 2, '(average cost: ${:.2f})'.format(concourse_B['total_paid_compostable']))
    
    
    worksheet.write(row+12, col, 'Total Revenue Before Sales Tax')
    worksheet.write(row+12, col + 1, '${:.2f}'.format(concourse_B['total_revenue_before_tax']))
    workbook.close() 

    return filepath


def getOrderDetails(concourse, order):
    if order['drop_bottle'] == True:  # compostable
        concourse['num_compostable_bottles'] += 1
        concourse['total_paid_compostable'] += order['drink_price']/100
    else: # refills
        if order['drink_price'] == 0:
            concourse['num_free_refills'] += 1
        else:
            concourse['num_paid_refills'] += 1
            concourse['total_paid_refills'] += order['drink_price']/100


def main():
    myclient = pymongo.MongoClient(URL)
    current_time = datetime.now() 
    print("Current time " + str(current_time))
    est = pytz.timezone('US/Eastern')
    pst = pytz.timezone('US/Pacific')
    current_time = current_time.astimezone(est)
    print("New current time " + str(current_time))
    # current_time = datetime(2020, 7, 31)  # test
    new_final_time = current_time + timedelta(days = -1) 
    first_day = new_final_time + timedelta(days= -7)
    

    # find prev Sunday
    while new_final_time.weekday() != 6:
        new_final_time = new_final_time + timedelta(days = -1) 
        first_day = new_final_time + timedelta(days= -7)
    
    utc = pytz.timezone('UTC')
    start = est.localize(datetime(first_day.year, first_day.month, first_day.day, 23, 59, 59))
    s = start
    
    
    end = est.localize(datetime(new_final_time.year, new_final_time.month, new_final_time.day, 23, 59, 59))
    e = end
    print('Start time is {} end time is {}'.format(start, end))
    start = start.astimezone(utc)
    end = end.astimezone(utc)
    print('Start time is {} end time is {}'.format(start, end))
    # print(start)
    # print(end)
    
    with myclient:
        mydb = myclient[DB]
        mycol = mydb["orders"]

        # query last week's orders
        orders = list(mycol.find({'date_created': {'$gte': start, '$lte': end}}).sort('date_created',pymongo.DESCENDING))
        print('order count is {}'.format(len(orders)))
        for order in orders:
            print('Order date is {}'.format(order['date_created'].astimezone((pst))))
            # print('Kiosk: {}'.format(order['kiosk_ID']))
            # print('Date created {}'.format(order['date_created']))
            # print(type(order['drop_bottle']))
            if order['kiosk_ID'] =='P3-2':  # A
                # print('A - P3-2 {}'.format(order['date_created'].astimezone((pst))))
                getOrderDetails(concourse_A, order)
            elif order['kiosk_ID'] == 'P3-1':  # B
                getOrderDetails(concourse_B, order)
                # print('A - P3-1 {}'.format(order['date_created'].astimezone((pst))))
                
            
    
    filepath = createExcelbooks(s, e)
    return filepath, start, end



def get_start_and_end_date():
    current_time = datetime.now() 
    print("Current time " + str(current_time))
    est = pytz.timezone('US/Eastern')
    pst = pytz.timezone('US/Pacific')
    current_time = current_time.astimezone(est)
    print("New current time " + str(current_time))
    # current_time = datetime(2020, 7, 31)  # test
    new_final_time = current_time + timedelta(days = -1) 
    first_day = new_final_time + timedelta(days= -7)
    

    # find prev Sunday
    while new_final_time.weekday() != 6:
        new_final_time = new_final_time + timedelta(days = -1) 
        first_day = new_final_time + timedelta(days= -7)
    
    utc = pytz.timezone('UTC')
    start = est.localize(datetime(first_day.year, first_day.month, first_day.day, 23, 59, 59))
    s = start
    
    
    end = est.localize(datetime(new_final_time.year, new_final_time.month, new_final_time.day, 23, 59, 59))
    e = end
    print('Start time is {} end time is {}'.format(start, end))
    start = start.astimezone(utc)
    end = end.astimezone(utc)
    print('Start time is {} end time is {}'.format(start, end))
    # print(start)
    # print(end)
    

    return start, end


def user_role_helper(user, mydb):
    print("user role helper: {}".format(user['role']))
    if user['role']  == 'drop-super-admin': # generate report for all orders in time frame
        print("drop_super_admin")
        order_collection = mydb['orders']
        start, end = get_start_and_end_date()
        orders = list(order_collection.find({'date_created': {'$gte': start, '$lte': end}}).sort('date_created',pymongo.DESCENDING))


        print("Num orders:  {}".format(len(orders)))

        pass
    elif user['role'] == 'drop-admin':  # same as above
        print('drop_admin')
        pass
    elif user['role'] == 'partner-admin': # generate report for all orders in that company
        pass 
    elif user['role'] == 'partner-user': # only for orders from kiosks in their "allowed list" #TODO
        pass 

    pass

def get_orders():
    print('Get orders called.')

    myclient = pymongo.MongoClient(URL)

    with myclient:

        mydb = myclient[DB]
        mycol = mydb["reportpreferences"]

        report_preferences = list(mycol.find({}))

        for pref in report_preferences:
            print("Preferece is {}".format(pref))

            user_collection = mydb["users"]
            user = user_collection.find_one({ 'uuid': pref['user']}, {'_id': 0, 'company_ID': 1, 'role': 1})

            user_role_helper(user, mydb)
            


            print("Company ID is: {}".format(user))

            kiosk_col = mydb['kiosks']
            kiosks = kiosk_col.find({ 'company_ID': user['company_ID']})

       

if __name__ == "__main__":
    # print('Creating report for {} environment'.format(ENV))
    # filepath, start, end = main()
    # end += timedelta(days = -1)
    # # recipient = 'gmulonga@dropwater.co'
    # recipient = 'acarper@cvgairport.com'
    # # recipient = 'britni@dropwater.co'
    # subject = 'Drop Water Weekly Summary {}/{}-{}/{}'.format(start.month, start.day, end.month, end.day)
    # body = 'Weekly Summary'
    # # send email
    # sendEmailSES(recipient, filepath, subject, body, start, end)
    print("Running reports daemon...")
    get_orders()

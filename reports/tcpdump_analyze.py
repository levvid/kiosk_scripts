#!/usr/local/bin/python
#
# parse tcpdump output file
# get total bytes
# percent total of all contributors
#
#  Open the tcpdump file
#####################################
# Instructions:
#
# Run tcpdump -D  get list of network devices
# find network device, eth0 for kiosks
#
# Run tcpdump -D eth0 > tcpdump.out
#
# Run this script with python3 
#
# tcpdump_analyze.py tcpdump.out
#
# NOTE: tcpdump, lsof must be installed on the
# box this script is run on
#
#####################################

import re
import math
import sys
import socket

from subprocess import Popen, PIPE, STDOUT

# this is the id of the server to analyze
serverId = socket.gethostname()

# dictionary for final report
rxtx = {}

# dictionary port to service
services_by_port = {}

# total byte count
total_byte_count = 0

#
# open file and load into lines string array
#
def read_file():
    lines = []
    line = ""
    print("INFO: opening {}".format(filename))
    with open(filename) as f:
        print("SUCCESS opening {}".format(filename))
        line = f.readline()
        lines.append(line)
        while line:
            line = f.readline()
            lines.append(line)

    return lines

#
# given dirty string, return int or 0
#
def clean_int(str):
    val = re.search("\d+", str)
    if val == None:
        return 0
    else:
        return int(val.group())

#
# given the length portion of the string, get the
# length as an int, or return 0
#
def get_byte_count(line):
    global total_byte_count
    parts = line.split(",")
    # find length
    for part in parts:
        if part.find("length") > 0:
            bits = part.split()
            byte_count = clean_int(bits[1])
            total_byte_count += byte_count
            return byte_count

    return 0

def is_char_string(str):
    robj = re.search("\d", str)
    if robj:
        return False
    else:
        return True

def get_service_from_port(port, protocol):
    if services_by_port.get(port) != None:
        return services_by_port.get(port)

    # Make sure port is a number otherwise
    # just return it
    robj = re.search("\d+", port)
    if robj == None:
        return port

    cmd = "lsof -i {}:{} -i {}:{} -Fc".format("tcp", port, "udp", port)
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
    output = p.stdout.read()
    str = output.decode("utf-8")
    lines = str.split("\n")
    for line in lines:
        ret = re.search("^[c].*", line)
        if ret:
            proc = ret.group()
            service = proc[1:]
            services_by_port[port] = service
            return service

    return ""

def get_server_port(segment):
    pos = segment.rfind(".")
    server = segment[0:pos]
    p = segment[pos + 1:]

    if is_char_string(p):
        port = p
    else:
        robj = re.search("\d+", p)
        port = robj.group()

    serverport = "{}:{}".format(server, port)
    return {"server": server, "port": port, "serverport": serverport, "service": ""}

def is_key(obj):
    server = obj.get("server")
    if server.lower().find(serverId.lower()) < 0:
        return False

    return True

def determine_key(obj, protocol):
    service = get_service_from_port(obj.get("port"), protocol)
    obj["service"] = service

    if service != None and len(service) > 0:
        return service

    return obj.get("serverport")

def get_key(obj1, obj2, protocol):
    if is_key(obj1):
        return determine_key(obj1, protocol)

    if is_key(obj2):
        return determine_key(obj2, protocol)

    return False

def dict_has_key(dict, key):
    x = dict.get(key)
    if x == None:
        return False

    return True

def update_count(dict, key, byte_count):
    obj = dict.get(key)
    count = obj.get("count")
    count += byte_count
    # obj.update({"count", count})
    obj["count"] = count
    return obj

def get_protocol(str):
    if str.find("UDP"):
        return "UDP"

    return "TCP"
#
# return the rx, receive, and transmit portion of the line
# returns dictionary of values
#
def load_rxtx(line):
    global rxtx
    if line.find(serverId.lower()) < 0 and line.find(serverId.upper()) < 0:
        return

    byte_count = get_byte_count(line)
    parts = line.split(",")
    bits = parts[0].split()
    if bits[1] == "ARP":
        return

    ipver = bits[1]
    protocol = get_protocol(line)
    sender = get_server_port(bits[2])
    receiver = get_server_port(bits[4]);
    key = get_key(sender, receiver, protocol)

    if dict_has_key(rxtx, key):
        rxtx[key] = update_count(rxtx, key, byte_count)
    else:
        rxtx[key] = {"sender": sender, "receiver": receiver, "ipver": ipver, "protocol": bits[5], "count": byte_count}

def print_report():
    print("TOTAL BYTES: {}".format(total_byte_count))
    keys = rxtx.keys()
    for key in keys:
        obj = rxtx.get(key)
        count = obj.get("count")
        service = obj.get("service")
        sender = obj.get("sender")
        receiver = obj.get("receiver")
        percent = 0

        if total_byte_count > 0:
            percent = math.trunc(count / total_byte_count * 100)

        if percent != 0:
            print("service: {} sender: {} receiver: {} bytes: {} percent: {}%".format(key, sender.get("serverport"), receiver.get("serverport"), count, percent))


def parse_lines(lines):
    linecnt = 0
    for line in lines:
        linecnt += 1;
        if linecnt % 100 == 0:
            print("Processed {} lines".format(linecnt))

        load_rxtx(line)

    print_report()
    print("Total size: {}".format(total_byte_count))

def run_stats():
    lines = read_file()
    parse_lines(lines)


# run it all
if len(sys.argv) < 2:
    print("main.py tcp-dump-file")
    exit(1)

filename = sys.argv[1]
run_stats()

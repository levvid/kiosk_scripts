#!/usr/bin/python

import websocket
import json, sys, time
from datetime import datetime
##### Simulates kiosk as WS client

userIdQuery = '?userId='
userId = ''
connectionId = ''

URL = 'wss://i93po5s0lh.execute-api.us-west-1.amazonaws.com/Prod'
# try:
#     import thread
# except ImportError:
#     import _thread as thread
# import time
def on_message(ws, message):
    curr_time = datetime.now()
    formatted_time = curr_time.strftime('%S')

    print('Message received {}'.format(formatted_time))

    print(message)
    data = json.loads(message)
    
    print('\n\n')
    
    if 'connectionId' in data:
        
        # Send kioskConnected status with self connectionId
        connectionId = data['connectionId']
        print("Set con Id to " + connectionId)
        msg = { 
            "action": "chat", 
            "data": { 
                'kioskConnectionId': connectionId, 
                'kioskConnected': True 
                }, 
            "recipientId": userId
        }

        ws.send(json.dumps(msg))
    elif 'dispenseStart' in data:
        msg =  {
            "action": "dispense", 
            "data": { 
                'dispenseStatus': 'startReceived',
                'uuid': data['uuid']
            }, 
            "recipientId": userId,
        }
        print('Sending dispense start received')
        ws.send(json.dumps(msg))
    elif 'isOrder' in data:
        order = data
        print('order is {}'.format(order))
        print(order['flavor'])
        if order['flavor'] != 'water':
            msg =  {
                "action": "chat", 
                "data": { 
                    'orderType': 'paid'
                    }, 
                "recipientId": userId
            }
            print("Sending paid")
            ws.send(json.dumps(msg))

            time.sleep(5)
            msg =  {
                "action": "chat", 
                "data": { 
                    'paymentStatus': 'complete'
                    }, 
                "recipientId": userId
            }
            print('payment status complete')
            ws.send(json.dumps(msg))

        else:
            print("Sending order type free")
            msg =  {
                "action": "chat", 
                "data": { 
                    'orderType': 'free'
                    }, 
                "recipientId": userId
            }

            ws.send(json.dumps(msg))
        
        if order['dropBottle'] == True:
            msg =  {
                "action": "chat", 
                "data": { 
                    'orderStatus': 'success'
                    }, 
                "recipientId": userId
            }
            time.sleep(10)
            print("Sending drop bottle complete.")
            ws.send(json.dumps(msg))
    

def on_error(ws, error):
    print("error occured")
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    print("Connected")
    connId = {
        "action": "getConnectionId"
    }
    ws.send(json.dumps(connId))

    print('Connection Id: ' + connectionId)


if __name__ == "__main__":
    userId = sys.argv[-1]
    print('User ID is {}'.format(userId))
    URL += userIdQuery + userId
    print('New URL is {}'.format(URL))
    # websocket.enableTrace(True)
    ws = websocket.WebSocketApp(URL,
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()

    
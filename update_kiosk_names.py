import pymongo
import urllib
from datetime import datetime, timedelta 
import xlsxwriter 
import pytz
from Email import sendEmailSES

STAGING_URL = "mongodb+srv://test_user:" + urllib.parse.quote_plus('drop@3507') + "@drop-water-bfrql.mongodb.net/DropWaterStaging?retryWrites=true&w=majority"
PROD_URL = "mongodb+srv://test_user:" + urllib.parse.quote_plus('drop@3507') + "@drop-water-bfrql.mongodb.net/DropWaterProduction?retryWrites=true&w=majority"
STAGING_DB = "DropWaterStaging"
PROD_DB = "DropWaterProduction"

ENV = 'Staging'
# ENV = 'Production'

if ENV == 'Staging':
    URL = STAGING_URL
    DB = STAGING_DB
else:
    URL = PROD_URL
    DB = PROD_DB

def main():
    myclient = pymongo.MongoClient(URL)
    
    with myclient:
        mydb = myclient[DB]
        mycol = mydb["orders"]

        myquery = { "kiosk_ID": "M0-1" }
        newvalues = { "$set": {"kiosk_ID": "M0-2" }}

        x = mycol.update_many(myquery, newvalues)
        print('Updated {} docs'.format(x.modified_count))

    
            
if __name__ == "__main__":
    main()

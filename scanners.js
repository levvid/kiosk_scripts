var HID = require("node-hid");



function registerScanner () {
    //faultManager.addActiveFault("scanner", "starting up", 1);
    var hidDevices = HID.devices();
    var deviceInfo = hidDevices.find( function(d) {
      var isScanner = d.vendorId===8208 && d.productId===30264;
      console.log('Is scanner')
      return isScanner;
    });
    if(deviceInfo) {
        console.log('Device info: ' + JSON.stringify(deviceInfo));
      var scannerDevice = new HID.HID(deviceInfo.path);
      
      //faultManager.clearActiveFaults("scanner");
      console.log('Scanner ' + JSON.stringify(scannerDevice));
      
    }
  };

  registerScanner();



var MongoClient = require('mongodb').MongoClient;

MongoClient.connect("mongodb://localhost:27017/reliability", function(err, db) {
  if (err) {
      console.log("Error connecting: " + JSON.stringify(err));
  }

  else {
      console.log("Connected to DB");
  }
});
  
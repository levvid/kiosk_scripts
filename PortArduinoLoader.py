# arduino bootloader from Raspberry Pi
# This program takes the most recent files and can upload them remotely to the module

import os
import serial
import time
import sys
import glob


os.system("pm2 stop all")
time.sleep(5)

port = raw_input("Which Port Are We Updating? \t0-7:\t")
print('Updating port /dev/ttyACM{}'.format(port))
portString = "/dev/ttyACM" + str(port)
try:
    ser = serial.Serial(port=portString, baudrate=9600, timeout=7)
    try:
        ser.write('?')
        moduleData = ser.read()
        print(moduleData)
        ser.close()

        
        bootloaderMode = serial.Serial(port=portString, baudrate=1200)
        bootloaderMode.close()
        time.sleep(1)
        os.system("avrdude -C/etc/avrdude.conf  -patmega32u4 -cavr109 -v -P" + portString + \
                    " -b57600 -D -V -Uflash:w:/home/dropwater/remotemoduleupdater/file.hex:i")
    except:
        print('Port ' + portString + ' not found')
except:
    print('Could not find port ' + str(portString))


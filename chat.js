// Copyright 2018-2020Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

const AWS = require('aws-sdk');

const ddb = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10', region: process.env.AWS_REGION });

const { TABLE_NAME } = process.env;

exports.handler = async event => {

    var recipientId = JSON.parse(event.body).recipientId;
    var postData = JSON.stringify(JSON.parse(event.body).data);
    console.log("Chat Start...");

    console.log(recipientId);
    const apigwManagementApi = new AWS.ApiGatewayManagementApi({
        apiVersion: '2018-11-29',
        endpoint: event.requestContext.domainName + '/' + event.requestContext.stage
    });
    try {
        console.log('Data: ' + postData);
        console.log("Sending to this ID: " + recipientId);
        await apigwManagementApi.postToConnection({ ConnectionId: recipientId, Data: postData }).promise();
    }
    catch (e) {
        if (e.statusCode === 410) {
            console.log(`Found stale connection, deleting ${recipientId}`);
            await ddb.delete({ TableName: TABLE_NAME, Key: { recipientId } }).promise();
        }
        else {
            throw e;
        }
    }
    
    return { statusCode: 200, body: 'Data sent.' };
};

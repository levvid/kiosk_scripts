#!/bin/bash
 
# Run this script in display 0 - the monitor
export DISPLAY=:0
 
# Hide the mouse from the display
unclutter &
xrandr -o left &
 
# Prevents screen blanking
xset s off 
xset s noblank
xset -dpms

gsettings set org.gnome.desktop.screensaver lock-enabled false
gsettings set org.gnome.desktop.session idle-delay 0

# If Chromium crashes (usually due to rebooting), clear the crash flag so we don't have the annoying warning bar
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' /home/dropwater/.config/chromium/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' /home/dropwater/.config/chromium/Default/Preferences
 
# Run Chromium and open tabs
# Legacy flags
# /usr/bin/chromium-browser --enable-logging=stderr --v=1 > log.txt 2>&1 --window-size=1920,1080 --kiosk --autoplay-policy=no-user-gesture-required --window-position=0,0 http://localhost:3000 &


/usr/bin/chromium-browser --kiosk --disable-pinch --overscroll-history-navigation=0 --enable-logging=stderr --v=1 > ~/logs/chromium-log 2>&1 --no-first-run --touch-events=enabled --fast --fast-start --disable-popup-blocking --disable-infobars --disable-session-crashed-bubble --disable-tab-switcher --disable-translate --enable-low-res-tiling --disable-features=TranslateUI --aggressive-cache-discard --disable-notifications --disk-cache-dir=/dev/null --enable-logging=stderr --v=1 > ~/logs/chromium_logs 2>&1 --window-size=1920,1080 --incognito --window-position=0,0 --autoplay-policy=no-user-gesture-required --app=http://localhost:3000  --remote-debugging-port=4000 --user-data-dir=remote-profile &
/usr/bin/chromium-browser --kiosk --disable-pinch --overscroll-history-navigation=0 --enable-logging=stderr --v=1 > ~/logs/chromium-log 2>&1 --no-first-run --touch-events=enabled --fast --fast-start --disable-popup-blocking --disable-infobars --disable-session-crashed-bubble --disable-tab-switcher --disable-translate --enable-low-res-tiling --disable-features=TranslateUI --aggressive-cache-discard --disable-notifications --disk-cache-dir=/dev/null --enable-logging=stderr --v=1 > ~/logs/chromium_logs 2>&1 --window-size=1920,1080 --incognito --window-position=0,0 --autoplay-policy=no-user-gesture-required --app=http://localhost:3000  --user-data-dir=remote-profile &

#/usr/bin/chromium-browser --disable-features=TranslateUI --aggressive-cache-discard --disable-notifications --disk-cache-dir=/dev/null --enable-logging=stderr --v=1 > ~/logs/chromium_logs 2>&1 --window-size=1920,1080 --kiosk --incognito --window-position=0,0 --app=http://localhost:3000 &

# New run chromium browser
#/usr/bin/chromium-browser --disable-features=TranslateUI --disable-infobars  --disable-translate  --disable-notifications --window-position=0,0 --window-size=1920,1080 --full-screen --kiosk --autoplay-p$
# Start the kiosk loop. This keystroke changes the Chromium tab
# To have just anti-idle, use this line instead:
# xdotool keydown ctrl; xdotool keyup ctrl;
# Otherwise, the ctrl+Tab is designed to switch tabs in Chrome
# #
while (true)
  do
   # xdotool keydown ctrl+Tab; xdotool keyup ctrl+Tab;
    sleep 15
done


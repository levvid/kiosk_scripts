#arduino bootloader from Raspberry Pi
#This program takes the most recent files and can upload them remotely to the module

import os
import serial
import time
import sys
import glob


os.system("pm2 stop all")
time.sleep(5)

module = raw_input("Which Module Are We Updating? ")
print(module)

portCount = 0
while portCount < 8:
    portString = "/dev/ttyACM" + str(portCount)
    try:
        ser = serial.Serial(port=portString,baudrate = 9600,timeout = 7)
    except: 
        portCount += 1
        print('Could not find port ' + str(portCount - 1))
        continue
    print("sending ? to port " + portString)
    ser.write('?')
    moduleData = ser.read()
    print(moduleData)
    if(moduleData.lower() == module.lower()):
        print("found a match at port " + portString)
        ser.close()
        #listPorts()
        try:
                bootloaderMode = serial.Serial(port=portString,baudrate=1200)
                bootloaderMode.close()                
                time.sleep(1)
                #os.system("avrdude -c arduino -C /home/dropwater/kiosk_scripts/avrdude.conf -patmega32u4 -cavr109 -v -v -v -v -P" + portString + " -b57600 -D -Uflash:w:/home/dropwater/kiosk_scripts$
                os.system("avrdude -C/etc/avrdude.conf  -patmega32u4 -cavr109 -v -P" + portString + " -b57600 -D -V -Uflash:w:/home/dropwater/kiosk_scripts/file.hex:i")
                #os.system("avrdude -p m32u4 -P" + portString + " -c avr109")
                break;
        except:
                print('Port ' + portString + ' not found')
    ser.close()
    portCount = portCount + 1
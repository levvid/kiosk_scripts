#!/bin/bash


redis-server &
cd /home/dropwater/pegasus/
source ~/.virtualenvs/kiosk/bin/activate
python manage.py  runserver 0.0.0.0:8000 &
celery -A pegasus worker --loglevel=DEBUG &
python manage.py runworker  tower lighting reception refill additive job_handler operation_manager deck gantry &

exit 0
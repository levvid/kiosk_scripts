import subprocess
import json
import datetime
import pymongo
import urllib
import socket

STAGING_URL = "mongodb+srv://test_user:" + urllib.quote_plus('drop@3507') + "@drop-water-bfrql.mongodb.net/DropWaterStaging?retryWrites=true&w=majority"
PROD_URL = "mongodb+srv://test_user:" + urllib.quote_plus('drop@3507') + "@drop-water-bfrql.mongodb.net/DropWaterProduction?retryWrites=true&w=majority"
STAGING_DB = "DropWaterStaging"
PROD_DB = "DropWaterProduction"
HOSTNAME = socket.gethostname()

# ENV = 'Staging'
ENV = 'Production'

if ENV == 'Staging':
    URL = STAGING_URL
    DB = STAGING_DB
else:
    URL = PROD_URL
    DB = PROD_DB



def save_daily_data(day_objs):
    myclient = pymongo.MongoClient(URL)
    
    with myclient:
        mydb = myclient[DB]
        mycol = mydb["kioskdatauses"]

        for day_obj in day_objs:
            mycol.insert_one(day_obj)
            print('saving: ' + str(day_obj))
    

def main():
    command = ['vnstat', '-i', 'eth0', '--json', 'd']
    p = subprocess.Popen(command, stdout=subprocess.PIPE)
    text = {}
    try:
        text = p.stdout.read()
        retcode = p.wait()
    except:
        pass
    
    # import pdb; pdb.set_trace()
    kiosk_data = json.loads(text)
    interfaces = kiosk_data['interfaces']
    day_objs = []
    for interface in interfaces:
        # current_interface = json.loads(interface)
        daily_data_usage = interface['traffic']['days']
        for daily_usage in daily_data_usage:
            day = daily_usage['date']['day']
            month = daily_usage['date']['month']
            year = daily_usage['date']['year']
            date = datetime.datetime(year, month, day)


            rx = daily_usage['rx']
            tx = daily_usage['tx']

            day_total = rx + tx 
            day_total = day_total
            
            day_obj = {
                "date": date,
                "rx": rx,
                "tx": tx,
                "day_total": day_total,
                "kiosk_ID": HOSTNAME
            }
            day_objs.append(day_obj)
            
            print("Date: {} Total: {}".format(date, day_total))
            break;


    save_daily_data(day_objs)



if __name__ == "__main__":
    main()
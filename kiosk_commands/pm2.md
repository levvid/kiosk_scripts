### PM2 -logrotate 
pm2 set pm2-logrotate:rotateInterval '59 59 23 * * 7'
pm2 set pm2-logrotate:max_size 20M
pm2 set pm2-logrotate:retain 7
pm2 set pm2-logrotate:compress false
pm2 set pm2-logrotate:dateFormat YYYY-MM-DD_HH-mm-ss
pm2 set pm2-logrotate:workerInterval 86400
pm2 set pm2-logrotate:rotateModule true



### check config 
pm2 config pm2-logrotate

import email, smtplib, ssl
import socket
import os, time
# import Image

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from os.path import basename
from datetime import datetime

SENDERNAME = 'Drop Water'
SENDERNAME = socket.gethostname() + ' Reboot Screenshot'

def main():
	subject = "Screenshot from " + socket.gethostname()
	body = socket.gethostname() + " screenshot\n\n"
	sender_email = "dropwater.menlo@gmail.com"
	dev_email = "gmulonga@dropwater.co"
	software = "software@dropwater.co"
	password = "drop@35077"
	today = datetime.now().strftime("%m/%d/%Y_%H:%M:%S_")
	os.system('/home/dropwater/kiosk_scripts/screenshot.sh')
	os.system('convert /home/dropwater/screenshots/screenshot.jpg -quality 50 /home/dropwater/screenshots/screenshot.jpg')
	receiver_email = dev_email

	# Create a multipart message and set headers
	message = MIMEMultipart()
	message["From"] = email.utils.formataddr((SENDERNAME, sender_email))
	message["To"] = receiver_email
	message["Subject"] = subject
	# message["Bcc"] = receiver_email

	# Add body to email
	message.attach(MIMEText(body, "plain"))

	filename = "/home/dropwater/screenshots/screenshot.jpg"  # In same directory as script

	# with Image.open(filename) as fil:
	# 	fil.save(filename,quality=85,optimize=True)

	with open(filename, "rb") as fil:
		part = MIMEApplication(fil.read(),Name=basename(filename))
	# After the file is closed
	part['Content-Disposition'] = 'attachment; filename="%s"' % basename(today + filename)
	# Add attachment to message and convert message to string
	message.attach(part)
	text = message.as_string()

	# Log in to server using secure context and send email
	context = ssl.create_default_context()
	with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
	    server.login(sender_email, password)
	    server.sendmail(sender_email, receiver_email, text)

	print('Screenshot sent to ' + receiver_email)
	print("Email sent to {} on {}".format(receiver_email, today))

if __name__ == '__main__':
	time.sleep(300)
	main()

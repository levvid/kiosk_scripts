import pymongo
import urllib
import json
import boto3  
import uuid

STAGING_URL = "mongodb+srv://test_user:" + urllib.parse.quote_plus('drop@3507') + "@drop-water-bfrql.mongodb.net/DropWaterStaging?retryWrites=true&w=majority"
PROD_URL = "mongodb+srv://test_user:" + urllib.parse.quote_plus('drop@3507') + "@drop-water-bfrql.mongodb.net/DropWaterProduction?retryWrites=true&w=majority"
STAGING_DB = "DropWaterStaging"
PROD_DB = "DropWaterProduction"
ENV = 'Staging'
# ENV = 'Production'

if ENV == 'Staging':
    URL = STAGING_URL
    DB = STAGING_DB
    BUCKET_NAME = 'dropwater-staging'
else:
    URL = PROD_URL
    DB = PROD_DB
    BUCKET_NAME = 'dropwater'


def main():
    myclient = pymongo.MongoClient(URL)
    s3 = boto3.resource('s3')

    new_interactions = []

    with myclient:
        mydb = myclient[DB]
        mycol = mydb["userinteractions"]
        interaction_collection = mydb['interactions']
        # query last week's orders
        user_interactions = list(mycol.find({}).sort('date_created',pymongo.DESCENDING))

        for interaction in user_interactions:
            events = interaction['events']
            filename = 'events/{}.json'.format(uuid.uuid4())
            s3object = s3.Object(BUCKET_NAME, filename)

            s3object.put(
                Body=(bytes(json.dumps(events).encode('UTF-8')))
            )
            print("This is the file name {}".format(filename))

            new_interaction = {
                'kiosk_ID': interaction['kiosk_ID'],
                'date_created': interaction['date_created'],
                'order_ID': interaction['order_ID'],
                'events': filename
            }

            new_interactions.append(new_interaction)


            # break
        saved_interactions = interaction_collection.insert_many(new_interactions)
        print('SAVED IDS: {}'.format(saved_interactions.inserted_ids))
            

if __name__ == "__main__":
    print('UX for {} environment'.format(ENV))
    main()
    
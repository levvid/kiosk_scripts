# Send screenshot every 6 hours
#0 */6 * * * python3 ~/kiosk_scripts/auto_screenshot.py >> ~/logs/auto_screenshot.log 2>&1


#0 */6 * * * python /home/dropwater/kiosk_scripts/s3screenshots.py >> ~/logs/auto_screenshot.log 2>&1
0 */6 * * * python /home/dropwater/kiosk_scripts/screenshots/s3screenshots.py >> ~/logs/auto_screenshot.log 2>&1



# then 3 minutes after
03 */6 * * * python /home/dropwater/kiosk_scripts/screenshots/s3screenshots.py >> ~/logs/auto_screenshot.log 2>&1
#03 */6 * * * python3 ~/kiosk_scripts/auto_screenshot.py >> ~/logs/auto_screenshot.log 2>&1
#03 */6 * * * python /home/dropwater/kiosk_scripts/s3screenshots.py >> ~/logs/auto_screenshot.log 2>&1


# send screen shot 5 mins after reboot
@reboot python3 /home/dropwater/kiosk_scripts/dev_auto_screenshot.py >> ~/logs/dev_auto_screenshot.log 2>&1


# update data usage
58 23 * * * python /home/dropwater/kiosk_scripts/kiosk_data_usage.py >> ~/logs/kiosk_data_usage.log 2>&1


# sync local and AWS Mongo DB's
01 * * * * python3 /home/dropwater/production_scripts/sync_local_and_AWS.py >> /home/dropwater/logs/sync_local_and_AWS.log 2>&1
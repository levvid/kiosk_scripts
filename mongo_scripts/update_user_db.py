import pymongo
import urllib
import socket
import logging
from decouple import config

ENV = config('ENV')

AWS_STAGING_URL = '{}{}{}'.format(config('AWS_STAGING_URL').split('<PWD>')[0],
                                  urllib.parse.quote_plus(config('AWS_DB_PASSWORD')),
                                  config('AWS_STAGING_URL').split('<PWD>')[1])

AWS_PRODUCTION_URL = '{}{}{}'.format(config('AWS_PRODUCTION_URL').split('<PWD>')[0],
                                     urllib.parse.quote_plus(config('AWS_DB_PASSWORD')),
                                     config('AWS_PRODUCTION_URL').split('<PWD>')[1])
LOCAL_DB = config('LOCAL_DB')
STAGING_DB = config('STAGING_DB')
PROD_DB = config('PROD_DB')


# ENV = 'Production'
logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%m/%d/%Y, %H:%M:%S', level=logging.NOTSET)

logger = logging.getLogger(__name__)


HOST_NAME = socket.gethostname()

if ENV == 'Staging':
    AWS_URL = AWS_STAGING_URL
    LOCAL_URL = LOCAL_DB
    DB = STAGING_DB
else:
    AWS_URL = AWS_PRODUCTION_URL
    LOCAL_URL = LOCAL_DB
    DB = PROD_DB


def clone_users_to_local(users):
    """
    Saves user to the local DB
    :param user:
    :return:
    """
    logger.debug('Cloning users to local DB...')
    logger.debug(users)
    myclient = pymongo.MongoClient(LOCAL_URL)

    with myclient:
        mydb = myclient[DB]
        my_collection = mydb['users']

        my_collection.insert_many(users)

        logger.debug('Inserted user to local DB {}'.format(users))


def get_aws_users_info():
    """
    Queries Kiosk collection on AWS DB where kiosk_ID == HOST_NAME and returns the user object
    :return:
    """
    logger.debug('retrieving user info from AWS')
    myclient = pymongo.MongoClient(AWS_URL)

    with myclient:
        mydb = myclient[DB]
        my_collection = mydb['users']

        users = my_collection.find()

        logger.debug('user query result {}'.format(users))
        return users



def test_get_collection(collection_name):
    """

    :param collection_name:
    :return:
    """
    logger.debug('retrieving {} info from AWS'.format(collection_name))
    myclient = pymongo.MongoClient(AWS_URL)

    with myclient:
        mydb = myclient[DB]
        my_collection = mydb[collection_name]

        collection_objects = list(my_collection.find())

        logger.debug('{} collection query result: {}'.format(collection_name, collection_objects))
        logger.debug('col objects are {}'.format(collection_objects))


def main() -> None:
    """
    Clones Kiosk collection from AWS to local MongoDB where kiosk_ID == HOST_NAME.
    Will need to be run every time there are any updates to the user collection either from
    the dashboard or user side

    :return:
    """

    users = get_aws_users_info()

    clone_users_to_local(users)

    logger.debug('Done syncing user collection on local DB with AWS. Goodbye!')


if __name__ == "__main__":
    print(main.__doc__)
    main()

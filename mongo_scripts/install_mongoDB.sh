#
# To run this script  (sudo -s for good luck, not needed)
#
# bash install_mongoDB.sh
#
#
echo "Import the public key used by the package management system"

wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -

echo "Create the /etc/apt/sources.list.d/mongodb-org-4.4.list file for Ubuntu 18.04 (Bionic):"

echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list

echo "Reload local package database"

sudo apt-get update

echo "Install the MongoDB packages."

sudo apt-get install -y mongodb-org

echo "pin the package at the currently installed version"

echo "mongodb-org hold" | sudo dpkg --set-selections
echo "mongodb-org-server hold" | sudo dpkg --set-selections
echo "mongodb-org-shell hold" | sudo dpkg --set-selections
echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
echo "mongodb-org-tools hold" | sudo dpkg --set-selections

echo "Starting MongoDB"

sudo systemctl start mongod

echo "Verify MongoDB has started successfully"

sudo systemctl status mongod

echo "Setup MongoDB to start on reboot"

sudo systemctl enable mongod

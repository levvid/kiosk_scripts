import pymongo
import urllib
import socket
import logging
from decouple import config

ENV = config('ENV')

AWS_STAGING_URL = '{}{}{}'.format(config('AWS_STAGING_URL').split('<PWD>')[0],
                                  urllib.parse.quote_plus(config('AWS_DB_PASSWORD')),
                                  config('AWS_STAGING_URL').split('<PWD>')[1])

AWS_PRODUCTION_URL = '{}{}{}'.format(config('AWS_PRODUCTION_URL').split('<PWD>')[0],
                                     urllib.parse.quote_plus(config('AWS_DB_PASSWORD')),
                                     config('AWS_PRODUCTION_URL').split('<PWD>')[1])
LOCAL_DB = config('LOCAL_DB')
STAGING_DB = config('STAGING_DB')
PROD_DB = config('PROD_DB')


# ENV = 'Production'
logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%m/%d/%Y, %H:%M:%S', level=logging.NOTSET)

logger = logging.getLogger(__name__)


HOST_NAME = socket.gethostname()

if ENV == 'Staging':
    AWS_URL = AWS_STAGING_URL
    LOCAL_URL = LOCAL_DB
    DB = STAGING_DB
else:
    AWS_URL = AWS_PRODUCTION_URL
    LOCAL_URL = LOCAL_DB
    DB = PROD_DB


def clone_kiosk_to_local(kiosk):
    """
    Saves kiosk to the local DB
    :param kiosk:
    :return:
    """
    logger.debug('Cloning kiosk to local DB...')
    logger.debug(kiosk)
    myclient = pymongo.MongoClient(LOCAL_URL)

    with myclient:
        mydb = myclient[DB]
        my_collection = mydb['kiosks']

        my_collection.insert_one(kiosk)

        logger.debug('Inserted kiosk to local DB {}'.format(kiosk))


def get_aws_kiosk_info():
    """
    Queries Kiosk collection on AWS DB where kiosk_ID == HOST_NAME and returns the kiosk object
    :return:
    """
    logger.debug('retrieving kiosk info from AWS')
    myclient = pymongo.MongoClient(AWS_URL)

    with myclient:
        mydb = myclient[DB]
        my_collection = mydb['kiosks']

        kiosk = my_collection.find_one({"kiosk_ID": HOST_NAME})

        logger.debug('kiosk query result: {}'.format(kiosk))
        return kiosk



def test_get_collection(collection_name):
    """

    :param collection_name:
    :return:
    """
    logger.debug('retrieving {} info from AWS'.format(collection_name))
    myclient = pymongo.MongoClient(AWS_URL)

    with myclient:
        mydb = myclient[DB]
        my_collection = mydb[collection_name]

        collection_objects = list(my_collection.find())

        logger.debug('{} collection query result: {}'.format(collection_name, collection_objects))
        logger.debug('col objects are {}'.format(collection_objects))


def main() -> None:
    """
    Clones Kiosk collection from AWS to local MongoDB where kiosk_ID == HOST_NAME.
    Will need to be run every time there are any updates to the kiosk collection either from
    the dashboard or kiosk side

    :return:
    """

    kiosk = get_aws_kiosk_info()

    clone_kiosk_to_local(kiosk)

    logger.debug('Done syncing kiosk collection on local DB with AWS. Goodbye!')


if __name__ == "__main__":
    print(main.__doc__)
    main()

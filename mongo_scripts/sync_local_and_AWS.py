import pymongo
import urllib
import logging
import platform
import subprocess
import traceback
from decouple import config
import copy
import time

ENV = config('ENV')

# List of collections to sync between AWS and the local DB
COLLECTIONS_TO_UPDATE = config('COLLECTIONS_TO_UPDATE').split(',')
COLLECTIONS_TO_INSERT = config('COLLECTIONS_TO_INSERT').split(',')
PING_ADDRESSES = config('PING_ADDRESSES').split(',')

AWS_STAGING_URL = '{}{}{}'.format(config('AWS_STAGING_URL').split('<PWD>')[0],
                                  urllib.parse.quote_plus(config('AWS_DB_PASSWORD')),
                                  config('AWS_STAGING_URL').split('<PWD>')[1])

AWS_PRODUCTION_URL = '{}{}{}'.format(config('AWS_PRODUCTION_URL').split('<PWD>')[0],
                                     urllib.parse.quote_plus(config('AWS_DB_PASSWORD')),
                                     config('AWS_PRODUCTION_URL').split('<PWD>')[1])
LOCAL_DB = config('LOCAL_DB')
STAGING_DB = config('STAGING_DB')
PROD_DB = config('PROD_DB')


logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%m/%d/%Y, %H:%M:%S', level=logging.NOTSET)

logger = logging.getLogger(__name__)


if ENV == 'Staging':
    AWS_URL = AWS_STAGING_URL
    LOCAL_URL = LOCAL_DB
    DB = STAGING_DB
else:
    AWS_URL = AWS_PRODUCTION_URL
    LOCAL_URL = LOCAL_DB
    DB = PROD_DB


def save_to_AWS(collection_name: str, object_list: list) -> bool:
    """
    Saves a list of objects <object_list> to collection_name on AWS.

    :param collection_name: name of collection to
    :param object_list:
    :return: inserted_ids: list of inserted Id's
    """
    new_object_list = copy.deepcopy(object_list)
    print("Saving to AWS for collection: {}".format(collection_name))
    myclient = pymongo.MongoClient(AWS_URL)
    with myclient:
        print('Db: {}'.format(DB))
        mydb = myclient[DB]
        my_collection = mydb[collection_name]

        if len(new_object_list) > 0:
            try:
                inserted_objects = my_collection.insert_many(new_object_list)
                logger.info('Successfully inserted objects to AWS with IDs: {}'.format(inserted_objects.inserted_ids))
            except Exception as e:
                logger.error(e)
                logger.error('Exception occurred while inserting to AWS DB.')
                return False
            finally:
                print('Done.')

            return True
        else:
            print('No items to insert to AWS DB.')
            return False


def update_to_AWS(collection_name: str, object_list: list) -> bool:
    """
    Saves a list of objects <object_list> to collection_name on AWS.

    :param collection_name: name of collection to
    :param object_list:
    :return: inserted_ids: list of inserted Id's
    """
    new_object_list = copy.deepcopy(object_list)
    print("Updating to AWS for collection: {}".format(collection_name))
    myclient = pymongo.MongoClient(AWS_URL)
    with myclient:
        mydb = myclient[DB]
        my_collection = mydb[collection_name]


        if len(new_object_list) > 0:
            for obj in new_object_list:
                try:
                    # obj.pop('kiosk_ID', None)
                    id = obj['_id']
                    obj.pop('_id', None)
                    kiosk_ID = obj['kiosk_ID']
                    obj.pop('kiosk_ID', None)

                    logger.info('Obj to be inserted is {}'.format(obj))
                    inserted_object = my_collection.update_one({"kiosk_ID": kiosk_ID }, { '$set': obj }, upsert=True)
                    logger.info('Successfully updated objects to AWS with IDs: {}'.format(inserted_object))
                except Exception as e:
                    logger.error(e)
                    logger.error('Exception occurred while inserting to AWS DB.')
                    return False
                finally:
                    print('Done.')

            return True
        else:
            print('No items to insert to AWS DB.')
            return False


def get_local_unsynced_data(collection_name: list) -> list:
    """
    Queries the local DB for all objects from <collection_name> where syncStatus == False

    :param collection_name:
    :return: List of collection objects
    """

    print('Getting local un_synced data for collection: {}'.format(collection_name))
    myclient = pymongo.MongoClient(LOCAL_URL)

    with myclient:
        mydb = myclient[DB]
        my_collection = mydb[collection_name]

        # query orders that have not been updated to AWS yet
        try:
            objects = list(my_collection.find({"syncStatus": False}))
            print('retrieved {} objects for collection {}'.format(len(objects), collection_name))
        except Exception as e:
            logger.error('retrieved {} objects for collection {}'.format(len(objects), collection_name))
            logger.error(e)
            pass

        return objects


def update_sync_status(collection_name: str, object_list: list) -> None:
    """
    Update the syncStatus flag in the local DB to True.

    :param collection_name:
    :param object_list:
    :return:
    """
    new_object_list = copy.deepcopy(object_list)
    print('Updating sync status on collection: {}'.format(collection_name))
    myclient = pymongo.MongoClient(LOCAL_URL)

    with myclient:
        mydb = myclient[DB]
        my_collection = mydb[collection_name]

        for obj in new_object_list:
            my_collection.find_one_and_update({"_id": obj['_id']}, {"$set": {"syncStatus": True}})
            print('Updated obj {} successfully'.format(obj))


def is_connected_helper(ip_address: str) -> bool:
    """

    :param ip_address:
    :return:
    """
    try:
        output = subprocess.check_output("ping -{} 1 {}".format('n' if platform.system().lower() == "windows" else 'c',
                                                                ip_address), shell=True, universal_newlines=True)
        logger.info('Ping output: {}'.format(output))
        if 'unreachable' in output:
            return False
        else:
            return True
    except Exception as e:
        logger.error(e)
        logger.error('Exception occurred while running is_connected.')
        return False
    pass


def is_connected() -> bool:
    """
    Ping's ip_address and returns the connection status. True == connected, False == not_connected
    :return:
    """
    connection_status = False
    for ip in PING_ADDRESSES:
        connection_status = is_connected_helper(ip)
        if not connection_status:
            logger.error('{} is unavailable.'.format(ip))
        else:
            logger.info('{} is available.'.format(ip))
    return connection_status


def main() -> None:
    """
    Syncs the local mongo database with the AWS mongo DB.
        1. Checks for kiosk internet connectivity status.
        2. Gets local un_synced collection objects.
        3. Saves the collection objects to AWS.
        4. Updates the syncStatus flag on the local DB to True.
    :return:
    """
    logger.info('Starting database sync.')

    count = 0
    if not is_connected():
        print('No internet connection detected. Quiting...')
        count += 1
        time.sleep(60)
        if count >= 4:
            return
    else:
        print("Connected to the internet.")

    for collection_name in COLLECTIONS_TO_UPDATE:
        collection_object_list = get_local_unsynced_data(collection_name)

        successfully_saved_to_aws = update_to_AWS(collection_name, collection_object_list)

        if successfully_saved_to_aws:
            update_sync_status(collection_name, collection_object_list)
            print('Successfully updated local DB.')
        else:
            print('Did not update local DB.')

    for collection_name in COLLECTIONS_TO_INSERT:
        collection_object_list = get_local_unsynced_data(collection_name)

        successfully_saved_to_aws = save_to_AWS(collection_name, collection_object_list)

        if successfully_saved_to_aws:
            update_sync_status(collection_name, collection_object_list)
            print('Successfully updated local DB.')
            print('Done saving to AWS.')
        else:
            print('Did not update local DB.')

    print('Done syncing local DB with AWS. Goodbye!')


if __name__ == "__main__":
    # print(main.__doc__)

    print('Syncing local and AWS Mongo DB')
    print('Db is {}'.format(DB))
    main()
    logger.info('Done syncing local DB.')

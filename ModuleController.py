#This program allows us to send individual commands
#to the individual modules to help recover from errors remotely

import os
import serial
import time


def readSerialPort():
    bytesToRead = ser.inWaiting()
    while(bytesToRead == 0):
        time.sleep(1)
        bytesToRead = ser.inWaiting()
    commandResponse = ser.read(bytesToRead)
    print(commandResponse)
    return commandResponse

def hasCapitalResponse(responseString):
  return any(char.isupper() for char in responseString)

# os.system("pm2 stop all")

module = raw_input("Which Module Are We Controlling? ")
print(module)

portCount = 0
while portCount < 3:
    portString = "/dev/ttyACM" + str(module)
    ser = serial.Serial(port=portString,baudrate = 9600,timeout = 4)
    print("sending ? to port " + portString)
    ser.write('?')
    moduleData = ser.read()
    print(moduleData)
    # if(moduleData.lower() == module.lower()):
    print("Writing to port " + portString)
    command = raw_input("What command would you like to give? ")
    ser.write(command)
    commandResponse = readSerialPort()
    while(hasCapitalResponse(commandResponse) == False):
        time.sleep(1)
        commandResponse = readSerialPort()
        nextCommand = ""
        while(nextCommand != "exit"):
            nextCommand = raw_input("Next command (type exit to quit) ")
            if(nextCommand != "exit"):
                ser.write(nextCommand)
                commandResponse = readSerialPort()
                while(hasCapitalResponse(commandResponse) == False):
                    time.sleep(1)
                    commandResponse = readSerialPort()
	ser.close()
        break;
    ser.close()
    portCount = portCount + 1


import email
import smtplib
import ssl
import base64


from datetime import datetime

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from os.path import basename
from datetime import datetime


# Replace sender@example.com with your "From" address.
# This address must be verified.
SENDER = 'reports@dropwater.co'
SENDERNAME = 'Drop Water'

# Replace recipient@example.com with a "To" address. If your account
# is still in the sandbox, this address must be verified.
# RECIPIENT  = 'gmulonga@dropwater.co'

# Replace smtp_username with your Amazon SES SMTP user name.
USERNAME_SMTP = "AKIA27ZCMDSP3XU3TAW4"

# Replace smtp_password with your Amazon SES SMTP password.
PASSWORD_SMTP = "BJ8l6dtB4AE86WEkOpP78zaRAqB+xwp7kG5sKZZJyGwL"

# (Optional) the name of a configuration set to use for this message.
# If you comment out this line, you also need to remove or comment out
# the "X-SES-CONFIGURATION-SET:" header below.
CONFIGURATION_SET = "DropWaterAWS_SES"

# If you're using Amazon SES in an AWS Region other than US West (Oregon),
# replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP
# endpoint in the appropriate region.
HOST = "email-smtp.us-west-2.amazonaws.com"
PORT = 587


def sendEmailSES(recipient, filepath, subject, body, start, end):
    # The subject line of the email.
    SUBJECT = subject
    today = datetime.now().strftime("%m/%d/%Y_%H:%M:%S_")
    # data_uri = base64.b64encode(open('drop_logo.png', 'rb').read()).decode('utf-8')
    # img_tag = '<img src="data:image/png;base64,{0}">'.format(data_uri)
    # The email body for recipients with non-HTML email clients.
    BODY_TEXT = body
    style = """
    <style>
        footer {
            text-align: center;
            padding: 3px;
            color: black;   
            font-style: italic;
            margin-bottom: 0px;
            margin-top: 30px;
        }
    </style>"""
    # The HTML body of the email.
    BODY_HTML = """<html>
    <head>{}</head>
    <body>
    <h3>Drop Water Weekly Summary {} - {}</h3>
    See attached. If you have any questions, please respond to this email.<br><br><br><br><br>
    <b>Drop Water</b><br>
    500 Howland Street, Suite 5, CA 94063<br>
    <b>Find us online at:</b> <a href="dropwater.co">Dropwater.co</a> | <a href="https://twitter.com/dropwater">Twitter</a> | <a href="https://www.facebook.com/dropwaterco">Facebook</a> | <a href="https://www.instagram.com/dropwaterco/">Instagram</a>
    <footer>
    <p>
    This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager. This message contains confidential information and is intended only for the individual named.
    </p>
    </footer>
    </body>
    </html>
    """.format(style, start.strftime("%m/%d"), end.strftime("%m/%d"))
    
    
    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = SUBJECT
    msg['From'] = email.utils.formataddr((SENDERNAME, SENDER))
    # msg['Cc'] = "glm83@cornell.edu"
    msg['Cc'] = 'software@dropwater.co'
    # msg['Bcc'] = 'software@dropwater.co'  # .format(start.strftime("%m/%d"), end.strftime("%m/%d"))
    msg['To'] = recipient
    msg["Bcc"] = 'gmulonga@dropwater.co'
    # Comment or delete the next line if you are not using a configuration set
    msg.add_header('X-SES-CONFIGURATION-SET', CONFIGURATION_SET)

    filename = filepath  # In same directory as script
    with open(filename, "rb") as fil:
        part3 = MIMEApplication(fil.read(), Name=basename(filename))

    # After the file is closed
    part3['Content-Disposition'] = 'attachment; filename="%s"' % basename(
        today + filename)
    # Add attachment to message and convert message to string
    msg.attach(part3)

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(BODY_TEXT, 'plain')
    part2 = MIMEText(BODY_HTML, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    # Try to send the message.
    try:
        server = smtplib.SMTP(HOST, PORT)
        server.ehlo()
        server.starttls()
        # stmplib docs recommend calling ehlo() before & after starttls()
        server.ehlo()
        server.login(USERNAME_SMTP, PASSWORD_SMTP)
        server.sendmail(SENDER, [recipient, msg['Cc'],
                                 msg['Bcc']], msg.as_string())
        server.close()
    # Display an error message if something goes wrong.
    except Exception as e:
        print("Error: ", e)
    else:
        print("Email sent to {} on {}".format(recipient, today))


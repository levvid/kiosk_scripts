function showSpinner() {
    console.log("Showing spinner");
    var opts = {
      lines: 20, // The number of lines to draw
      length: 80, // The length of each line
      width: 16, // The line thickness
      radius: 80, // The radius of the inner circle
      scale: 1.2, // Scales overall size of the spinner
      corners: 1, // Corner roundness (0..1)
      speed: 1, // Rounds per second
      rotate: 56, // The rotation offset
      animation: 'spinner-line-fade-default', // The CSS animation name for the lines
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#669ffa', // CSS color or array of colors
      fadeColor: 'transparent', // CSS color or array of colors
      top: '50%', // Top position relative to parent
      left: '50%', // Left position relative to parent
      shadow: '0 0 1px transparent', // Box-shadow for the lines
      zIndex: 2000000000, // The z-index (defaults to 2e9)
      className: 'spinner', // The CSS class to assign to the spinner
      position: 'absolute', // Element positioning
    };

    var target = document.getElementById('loader');
    target.style.display = "inline-block";

    var spinner = new Spinner(opts).spin();
    target.appendChild(spinner.el);
    
  }

  function hideSpinner() {
    console.log("Hiding spinner");
    var target = document.getElementById('loader');
    target.style.display = "none";
  }
import pymongo
import urllib

STAGING_URL = "mongodb+srv://test_user:" + urllib.parse.quote_plus('drop@3507') + "@drop-water-bfrql.mongodb.net/DropWaterStaging?retryWrites=true&w=majority"
PROD_URL = "mongodb+srv://test_user:" + urllib.parse.quote_plus('drop@3507') + "@drop-water-bfrql.mongodb.net/DropWaterProduction?retryWrites=true&w=majority"
STAGING_DB = "DropWaterStaging"
PROD_DB = "DropWaterProduction"

# ENV = 'Staging'
ENV = 'Production'

if ENV == 'Staging':
    URL = STAGING_URL
    DB = STAGING_DB
else:
    URL = PROD_URL
    DB = PROD_DB

def main():
    myclient = pymongo.MongoClient(URL)
    
    with myclient:
        mydb = myclient[DB]
        mycol = mydb["orders"]

        # query last week's orders
        orders = list(mycol.find({}))

        old_orders = list(mycol.find({"order_type": {"$exists": False}}))

        
        query_drop_bottle = { "order_type": {"$exists": False}, "drop_bottle": True }
        query_refill = { "order_type": {"$exists": False}, "drop_bottle": False }

        drop_bottle_order_type = { "$set": { "order_type": "Drop Bottle" } }
        refill_order_type = { "$set": { "order_type": "Screen Refill" } }

        updated_drop_bottle = mycol.update_many(query_drop_bottle, drop_bottle_order_type)
        updated_refill = mycol.update_many(query_refill, refill_order_type)


        print("Updated {} Drop Bottle orders.".format(updated_drop_bottle.modified_count))
        print("Updated {} Refill orders.".format(updated_refill.modified_count))
        
        # for order in old_orders:
        #     if order['drop_bottle'] == True:
        #         print(order)
        #     else:
        #         print(order)

        #     print(order)
        # print('Num orders old: {}'.format(len(old_orders)))
        # print('Num orders all: {}'.format(len(orders)))

if __name__ == "__main__":
    main()

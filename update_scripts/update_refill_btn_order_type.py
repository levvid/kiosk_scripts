import pymongo
import urllib

STAGING_URL = "mongodb+srv://test_user:" + urllib.parse.quote_plus('drop@3507') + "@drop-water-bfrql.mongodb.net/DropWaterStaging?retryWrites=true&w=majority"
PROD_URL = "mongodb+srv://test_user:" + urllib.parse.quote_plus('drop@3507') + "@drop-water-bfrql.mongodb.net/DropWaterProduction?retryWrites=true&w=majority"
STAGING_DB = "DropWaterStaging"
PROD_DB = "DropWaterProduction"

ENV = 'Staging'
# ENV = 'Production'

if ENV == 'Staging':
    URL = STAGING_URL
    DB = STAGING_DB
else:
    URL = PROD_URL
    DB = PROD_DB

def main():
    myclient = pymongo.MongoClient(URL)
    
    with myclient:
        mydb = myclient[DB]
        mycol = mydb["orders"]

        # query last week's orders
        orders = list(mycol.find({}))

        old_orders = list(mycol.find({"order_type": {"$exists": False}}))

        
        query_refill_btn = { "order_type": "Screen Refill", "drop_bottle": False }
        refill_order_type = { "$set": { "order_type": "Button Refill" } }

        # print(len(refill_order_type))
        print(len(list(mycol. find(query_refill_btn))))

        # updated_refill = mycol.update_many(query_refill_btn, refill_order_type)

        # print("Updated {} Refill orders.".format(updated_refill.modified_count))
        
        # for order in old_orders:
        #     if order['drop_bottle'] == True:
        #         print(order)
        #     else:
        #         print(order)

        #     print(order)
        # print('Num orders old: {}'.format(len(old_orders)))
        # print('Num orders all: {}'.format(len(orders)))

if __name__ == "__main__":
    main()

import boto3
import socket
import os
import uuid
import urllib
import pymongo
import base64
import json

from datetime import datetime

AWS_ACCESS_KEY="AKIAJJDRQVGQEPOYPIHQ"
AWS_SECRET_KEY="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"
HOST_NAME = socket.gethostname()



STAGING_URL = "mongodb+srv://test_user:" + urllib.quote_plus('drop@3507') + "@drop-water-bfrql.mongodb.net/DropWaterStaging?retryWrites=true&w=majority"
PROD_URL = "mongodb+srv://test_user:" + urllib.quote_plus('drop@3507') + "@drop-water-bfrql.mongodb.net/DropWaterProduction?retryWrites=true&w=majority"
STAGING_DB = "DropWaterStaging"
PROD_DB = "DropWaterProduction"
BUCKET_NAME = 'dropwater-staging'

# ENV = 'Staging'
ENV = 'Production'

if ENV == 'Staging':
    URL = STAGING_URL
    DB = STAGING_DB
else:
    URL = PROD_URL
    DB = PROD_DB
    BUCKET_NAME = 'dropwater'


def invokeLambdaFunction(functionName, payload):
    if  functionName == None:
        raise Exception('ERROR: functionName parameter cannot be NULL')
    payloadStr = json.dumps(payload)
    payloadBytesArr = bytes(payloadStr)
    client = boto3.client('lambda', 
    region_name="us-west-2",
    aws_access_key_id=AWS_ACCESS_KEY,
    aws_secret_access_key=AWS_SECRET_KEY,)
    response = client.invoke(
        # FunctionName=functionName,
        # InvocationType='Event',
        # LogType='Tail',
        # ClientContext='string',
        # Payload='',
        FunctionName=functionName,
        InvocationType='Event',
        LogType='None',
        Payload=json.dumps(payload)
        # Qualifier='string'
    )
    return response


def take_screenshot():
    os.system('/home/dropwater/kiosk_scripts/screenshots/screenshot.sh')
    os.system('convert /home/dropwater/screenshots/screenshot.jpg -quality 50 /home/dropwater/screenshots/screenshot.jpg')
    print('test')


def send_screenshot_email():
    os.system('/home/dropwater/kiosk_scripts/screenshots/send_screenshot_email.sh')


def saveToDB(name):
    myclient = pymongo.MongoClient(URL)
    s3 = boto3.resource('s3')

    new_interactions = []

    with myclient:
        mydb = myclient[DB]
        mycol = mydb["kioskscreenshots"]
        new_screenshot_obj = { "name": name, "kiosk_ID": HOST_NAME, "date_created":  datetime.utcnow() }

        x = mycol.insert_one(new_screenshot_obj)

        print("Successfully inserted {} into DB".format(new_screenshot_obj))
    
    # send shots in email
    # to be deprecated when AWS lambda fn is set up
    send_screenshot_email()


def main():
    # connect to s3
    s3 = boto3.resource(
    's3',
    region_name='us-west-2',
    aws_access_key_id=AWS_ACCESS_KEY,
    aws_secret_access_key=AWS_SECRET_KEY
    )

    take_screenshot()
    # get the screenshot
    name = str(uuid.uuid4())  # unique name for the screenshot
    with open('/home/dropwater/screenshots/screenshot.jpg', "rb") as new_screenshot:
        encoded_screenshot = base64.b64encode(new_screenshot.read())
        s3.Object(BUCKET_NAME, 'kiosk_screenshots/{}.jpg'.format(name)).put(Body=encoded_screenshot)

    
    # save to DB
    saveToDB(name)
    return name


if __name__ == '__main__':
    name = main()

    # # send email
    payloadObj = {
        "kiosk_ID": HOST_NAME,
        "name": name
    }
    response = invokeLambdaFunction(functionName='queryMongo',  payload=payloadObj)
    print('response: {}'.format(response))
    # take_screenshot()